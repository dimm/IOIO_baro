#!/usr/bin/python
import csv
from lxml import etree
from pykml.factory import KML_ElementMaker as KML

name_object = KML.name("")

_doc = KML.kml()

doc = etree.SubElement(_doc, 'Document')

fld = KML.Folder(KML.name("Name"))

coords = []

with open('2013-12-14-3.log', 'rb') as csvfile:
  lvareader = csv.reader(csvfile, delimiter=',')
  next(lvareader, None) # skip header

  cur_row = None

  for row in lvareader:
  	if(cur_row != None):
	  	coord = "\n"+cur_row[7]+","+cur_row[6]+","+cur_row[5]+"\n"+row[7]+","+row[6]+","+row[5]+"\n"
		pm = KML.Placemark(
		    KML.name(""),
		    KML.Style(
		    	KML.LineStyle(
		    		KML.color("ff00ffff"),
		    		KML.width(3)
		    	)
		    ),
			KML.LineString(
				KML.extrude(0),
                KML.altitudeMode("absolute"),
            	KML.coordinates(coord)
			)
		)
		fld.append(pm)
  	cur_row = row


doc.append(fld)
print etree.tostring(_doc, pretty_print=True)
