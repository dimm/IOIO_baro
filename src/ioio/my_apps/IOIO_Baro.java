package ioio.my_apps;

import ioio.lib.api.DigitalInput;
import ioio.lib.api.TwiMaster;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;
import ioio.lib.util.android.IOIOActivity;
import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
public class IOIO_Baro extends IOIOActivity {
	private TextView phystextView_, phystextHumView_, coordView_;
	private static DecimalFormat df;
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
	private static BufferedWriter writer;
	
	private static volatile double tempInt, tempExt, pres, hum, lat, lon, alt;
	
	static {
		NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
		df = (DecimalFormat)nf;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		df.setMaximumFractionDigits(6);
		df.setGroupingUsed(false);

		phystextView_ = (TextView)findViewById(R.id.PhystextView);
		phystextHumView_ = (TextView)findViewById(R.id.PhystextHumView);
		coordView_ = (TextView)findViewById(R.id.CoordView);

		LocationManager mlocManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		LocationListener mlocListener = new MyLocationListener();
		mlocManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 0, 0, mlocListener);
		
	}

	public void onLogClicked(View view) {
	    boolean on = ((ToggleButton) view).isChecked();
	    try {
		    if (on) {
		    	if(writer == null) {
		    		File logsDir = getExternalFilesDir("/IOIO_Baro/mylogs");
		    		if(!logsDir.exists()) {
		    			logsDir.mkdirs();
		    		}

		    		int numFile = 0;
		    		String curDate = dateFormat.format(Calendar.getInstance().getTime());
		    		File logFile;
		    		while((logFile = new File(logsDir, curDate+"-"+numFile+".log")).exists()) {
		    			numFile++;
		    		}
		    		writer = new BufferedWriter(new FileWriter(logFile));
		    		writer.write("#Time,TInt,TExt,Humid,Pres,Alt_Pres,Lat,Lon,Alt_GPS\n");
		    	}
		    } else {
		    	writer.close();
		    	writer = null;
		    }
	    } catch(Exception ex) {
	    	ex.printStackTrace();
	    }
	}
	
	class IOIOLooper extends BaseIOIOLooper {
		private TwiMaster twi_, twi2_;
		private DigitalInput EOC_pin;
		private byte[] dummy = new byte[1];
		private final byte BMP085deviceID = (byte) 0x77; // I2C ID is 0xEE >> 1
		private final byte[] requestChipID = new byte[] { (byte) 0xD0 };
		private byte[] responseChipID = new byte[1];
		private final byte[] requestParameters = new byte[] { (byte) 0xAA };
		private byte[] responseParameters = new byte[22];
		private final byte[] start_T = new byte[] { (byte)0xF4, (byte)0x2E };
		private byte[] start_P = new byte[] { (byte)0xF4, (byte)0xf4 }; // osrs=0 : 0x34, osrs=1 : 0x74, osrs=2 : 0xB4, osrs=3 : 0xF4
		private int osrs=1;
		private final byte[] request_Val = new byte[] { (byte) 0xF6 };
		private byte[] response_T = new byte[2];
		private byte[] response_P = new byte[3];
		private int up, ut;
		
		private byte[] request_H = new byte[] { 0x01, 0x02, 0x03, 0x04, 0x05 };
	    private byte[] response_H = new byte[4];

		
		public void setup() throws ConnectionLostException {
			try {
				twi_ = ioio_.openTwiMaster(2, TwiMaster.Rate.RATE_1MHz, false); // pin26: DA, pin25: CL
				EOC_pin = ioio_.openDigitalInput(27); // end of conversion == 1

				twi2_ = ioio_.openTwiMaster(1, TwiMaster.Rate.RATE_1MHz, false);
				
				try {
					// read the Chip ID from the BMP085
					// the result in responseChipID must be 85, otherwise something is wrong
					twi_.writeRead(BMP085deviceID, false, requestChipID, requestChipID.length, responseChipID, responseChipID.length);
					// read the 22 bytes of calibration coefficients from the sensor
					twi_.writeRead(BMP085deviceID, false, requestParameters, requestParameters.length, responseParameters, responseParameters.length);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				String stat = "ChipID: " + Byte.toString(responseChipID[0]) +";\nCalib coefficients: ";
				int i;
				for (i=0; i<responseParameters.length; i++) {
					stat = stat + Integer.toString(i)+ ": 0x" +Integer.toHexString(responseParameters[i]&0xff)+ ", ";
				}
				stat = stat + "\nCalib coefficients: ";
				for (i=0; i<responseParameters.length; i++) {
					stat = stat + Integer.toString(i)+ ": " +Integer.toString(responseParameters[i]&0xff)+ ", ";
				}
			} catch (ConnectionLostException e) {
				throw e;
			}
		}

		public void loop() throws ConnectionLostException {
			try {
				twi2_.writeRead(0x27, false, request_H, request_H.length,response_H,response_H.length);
//				int status = (response_H[0] & 0xc0) >> 6;
				hum = (((response_H[0] & 0x3f) << 8) + response_H[1]) * 100.0 / 16383.0;
				tempExt = ((response_H[2] << 6) + ((response_H[3] & 0xfc) >> 2)) * 165.0 / 16383.0 - 40.0;

				StringBuffer tpsH = new StringBuffer("T:"+ df.format(tempExt));
				tpsH.append(" C, H:"+ df.format(hum)+"%");
				setPhysHText(tpsH.toString());
				
				// calculate the measurement command from the over-sampling rate
				start_P[1] = (byte)((byte)(osrs <<6 ) | (byte)0x34);

				try {
					// trigger the temperature conversion
					twi_.writeRead(BMP085deviceID, false, start_T, start_T.length, dummy, 0);
					//				sleep(2); // make sure the conversion has started
					// wait until the conversion has completed, that makes the EOC pin of the BMP085 go high
					EOC_pin.waitForValue(true);
					// read the result of the temperature measurement
					twi_.writeRead(BMP085deviceID, false, request_Val, request_Val.length, response_T, response_T.length);

					// trigger the pressure conversion
					twi_.writeRead(BMP085deviceID, false, start_P, start_P.length, dummy, 0);
					//				sleep(2); // make sure the conversion has started
					// wait until the conversion has completed, that makes the EOC pin of the BMP085 go high
					// according to the data sheet, this takes between 4.5 and 25.5 msec depending on oversampling mode
					EOC_pin.waitForValue(true);
					// read the result of the pressure measurement
					twi_.writeRead(BMP085deviceID, false, request_Val, request_Val.length, response_P, response_P.length);

					// convert the raw sensor data into physical values in C and hPa
					// the calculation is based on the data sheet and a sample API on the Bosch web site:
					// http://www.bosch-sensortec.com/content/language1/html/3477.htm
					ut = ((response_T[0]&0xff)<<8) | (response_T[1]&0xff);
					up = (((response_P[0]&0xff)<<16) | ((response_P[1]&0xff)<<8) | (response_P[2]&0xff)) >> (8-osrs);
					int b5 = bmp085_get_b5(responseParameters,ut);
					String tps = "T:"+ df.format(bmp085_get_temperature(ut,b5)/10.0);
					tps = tps +" C, P:"+ df.format(bmp085_get_pressure(responseParameters,up,osrs,b5) / 100.0);
					tps = tps +" hPa";
					setPhysText(tps);
					tempInt = bmp085_get_temperature(ut,b5)/10.0;
					pres = bmp085_get_pressure(responseParameters,up,osrs,b5) / 100.0;
					
					if(null != writer)
						writer.write(System.currentTimeMillis()/1000+
								","+df.format(tempInt)+
								","+df.format(tempExt)+
								","+df.format(hum)+
								","+df.format(pres)+
								","+SensorManager.getAltitude(SensorManager.PRESSURE_STANDARD_ATMOSPHERE, (float)pres)+
								","+df.format(lat)+
								","+df.format(lon)+
								","+df.format(alt)+"\n");
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				Thread.sleep(500);
			} catch (InterruptedException e) {
				ioio_.disconnect();
			} catch (ConnectionLostException e) {
				throw e;
			}
		}
	}

	public class MyLocationListener implements LocationListener {
		@Override
		public void onLocationChanged(final Location loc) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					coordView_.setText("Height: "+df.format(loc.getAltitude()));
					lat = loc.getLatitude();
					lon = loc.getLongitude();
					alt = loc.getAltitude();
				}
			});
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}/* End of Class MyLocationListener */
	
	@Override
	protected IOIOLooper createIOIOLooper(){
		return new IOIOLooper();
	}

	private void setPhysText(final String str) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				phystextView_.setText(str);
			}
		});
	}

	private void setPhysHText(final String str) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				phystextHumView_.setText(str);
			}
		});
	}

	private int bmp085_get_b5(byte[] cal, int ut) 
	{
		int param_b5;
		int x1,x2;

		int ac5 = ((cal[8]&0xff)<<8) | (cal[9]&0xff);
		int ac6 = ((cal[10]&0xff)<<8) | (cal[11]&0xff);
		short mc = (short)(((cal[18]&0xff)<<8) | (cal[19]&0xff));
		short md = (short)(((cal[20]&0xff)<<8) | (cal[21]&0xff));

		x1 = ((ut - ac6) * ac5) >> 15;
		x2 = ((int)mc << 11) / (x1 + (int)md);
		param_b5 = x1 + x2;

		return (param_b5);
	}

	private int bmp085_get_temperature(int ut, int param_b5) 
	{
		int temperature;

		temperature = ((param_b5 + 8) >> 4);  // temperature in 0.1�C

		return (temperature);
	}

	private int bmp085_get_pressure(byte[] cal, int up, int osrs, int param_b5)
	{
		int pressure,x1,x2,x3,b3,b6;
		int b4, b7;

		short ac1 = (short)(((cal[0]&0xff)<<8) | (cal[1]&0xff));
		short ac2 = (short)(((cal[2]&0xff)<<8) | (cal[3]&0xff));
		short ac3 = (short)(((cal[4]&0xff)<<8) | (cal[5]&0xff));
		int   ac4 =        (((cal[6]&0xff)<<8) | (cal[7]&0xff));
		short b1 =  (short)(((cal[12]&0xff)<<8) | (cal[13]&0xff));
		short b2 =  (short)(((cal[14]&0xff)<<8) | (cal[15]&0xff));

		b6 = param_b5 - 4000;
		//*****calculate B3************
		x1 = (b6*b6) >> 12;	 	 
		x1 *= b2;
		x1 >>=11;

		x2 = ((int)ac2*b6);
		x2 >>=11;

		x3 = x1+x2;

		b3 = (((((int)ac1 )*4 + x3) << osrs) + 2) >> 2;

		//*****calculate B4************
		x1 = ((int)ac3* b6) >> 13;
		x2 = ((int)b1 * ((b6*b6) >> 12) ) >> 16;
		x3 = ((x1 + x2) + 2) >> 2;
		b4 = (ac4 *(x3 + 32768)) >> 15;

		b7 = ((up - b3) * (50000>>osrs));   
		if (b7 < 0x80000000)
		{
			pressure = (b7 << 1) / b4;
		}
		else
		{ 
			pressure = (b7 / b4) << 1;
		}

		x1 = pressure >> 8;
			x1 *= x1;

			final int SMD500_PARAM_MG=3038;        //calibration parameter
			final int SMD500_PARAM_MH=-7357;        //calibration parameter
			final int SMD500_PARAM_MI=3791;        //calibration parameter

			x1 = (x1 * SMD500_PARAM_MG) >> 16;
			x2 = (pressure * SMD500_PARAM_MH) >> 16;
		pressure += (x1 + x2 + SMD500_PARAM_MI) >> 4;	// pressure in 0.01 hPa  

					return (pressure);
	}
}
